package com.example.springreport.util;

import com.example.springreport.entity.Payment;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GeneratePdfReport {


    public static ByteArrayInputStream paymentsReport(List<Payment> payments) {
        Document document = new Document(PageSize.A4.rotate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            document.setMargins(5, 5, 25, 10);

            PdfWriter writer = PdfWriter.getInstance(document, out);




            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(90);
            table.setWidths(new int[]{1, 3, 2, 1, 2});

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12);
            Font defaultFont = FontFactory.getFont(FontFactory.HELVETICA, 12);

            PdfPCell hcell;

            hcell = new PdfPCell(new Phrase("Id", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Product Name", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Price", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Q", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Total", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            PdfPCell cell;
            Long sumTotal = 0L;

            for (Payment payment : payments) {

                sumTotal = sumTotal + payment.getTotal();

                cell = new PdfPCell(new Phrase(payment.getId().toString(), defaultFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(payment.getProductName(), defaultFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Rectangle.LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("$" + payment.getPrice().toString(), defaultFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(payment.getQuantity().toString(), defaultFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("$" + payment.getTotal().toString(), defaultFont));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
                table.addCell(cell);

            }

            cell = new PdfPCell(new Phrase("Total", headFont));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Rectangle.LEFT);
            cell.setColspan(4);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("$" + sumTotal.toString(), defaultFont));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            //HeaderFooter headerFooter = new HeaderFooter();
            //writer.setPageEvent(headerFooter);


            document.open();

            GenerateHeader(document, writer);

            document.add(table);

            GenerateFooter(document, writer);

            document.close();

        } catch (DocumentException ex) {
            Logger.getLogger(GeneratePdfReport.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());

    }

    static void GenerateHeader(Document document, PdfWriter writer) {
        try {
            // START HEADER
            Font headerFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 16);

            Phrase headerText = new Phrase("PLUSMiles Operations Tracking Report (OTR)", headerFont);

            Image img = null;
            Image img2 = null;

            img = Image.getInstance(GeneratePdfReport.class.getClassLoader().getResource("so.png").getPath());
            img2 = Image.getInstance(GeneratePdfReport.class.getClassLoader().getResource("so.png").getPath());
            img.scaleToFit(100,90);
            img2.scaleToFit(100,90);

            PdfPTable table2 = new PdfPTable(3);
            table2.setWidthPercentage(90);
            table2.setWidths(new float[] { 2, 5, 2});
            table2.setSpacingBefore(15f);
            table2.setSpacingAfter(25f);

            PdfPCell cell2;

            cell2 = new PdfPCell(img);

            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(Rectangle.ALIGN_LEFT);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table2.addCell(cell2);

            cell2 = new PdfPCell(headerText);
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table2.addCell(cell2);

            cell2 = new PdfPCell(img2);
            cell2.setBorder(Rectangle.NO_BORDER);
            cell2.setHorizontalAlignment(Rectangle.ALIGN_RIGHT);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table2.addCell(cell2);


            document.open();
            document.add(table2);
            document.add(Chunk.NEWLINE);
            // END HEADER
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static void GenerateFooter(Document document, PdfWriter writer) {
        Font defaultFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 11);
        try {

            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(90);
            table.setWidths(new float[] { 2, 5, 2 });
            table.setSpacingBefore(20f);

            PdfPCell emptyCell = new PdfPCell(new Phrase(""));
            emptyCell.setBorder(Rectangle.NO_BORDER);

            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Redeemed by:", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setRowspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Redeemed by:", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("SA on-duty", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Supervisor on-duty", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);

            Chunk chunk1 = new Chunk("");
            chunk1.setUnderline(1.5f, -1);

            cell = new PdfPCell(new Phrase(chunk1));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setVerticalAlignment(Rectangle.BOTTOM);
            cell.setColspan(3);
            cell.setFixedHeight(30f);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Signature", defaultFont));

            cell.setBorder(Rectangle.TOP);

            table.addCell(cell);
            emptyCell.setRowspan(4);
            table.addCell(emptyCell);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Name :", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            //table.addCell(emptyCell);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Staff ID :", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            //table.addCell(emptyCell);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Date :", defaultFont));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
            //table.addCell(emptyCell);
            table.addCell(cell);

            document.add(table);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static class HeaderFooter extends PdfPageEventHelper {
        @Override
        public void onChapterEnd(PdfWriter writer, Document document, float position) {
            super.onChapterEnd(writer, document, position);
        }

        @Override
        public void onCloseDocument(PdfWriter writer, Document document) {
            super.onCloseDocument(writer, document);
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            super.onEndPage(writer, document);
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            super.onStartPage(writer, document);
        }

        @Override
        public void onOpenDocument(PdfWriter writer, Document document) {
            super.onOpenDocument(writer, document);
        }
    }

}
