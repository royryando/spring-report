package com.example.springreport.service;

import com.example.springreport.entity.Payment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Override
    public List<Payment> paymentList() {
        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(1, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(2, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(3, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(4, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(5, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(6, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(7, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(8, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(9, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(10, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(11, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(12, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(13, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(14, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(15, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(16, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(17, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(18, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(19, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(20, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(21, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(22, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(23, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(24, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(25, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(26, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(27, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(28, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(29, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(30, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(31, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(32, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(33, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(34, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(35, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(36, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(37, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(38, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(39, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(40, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(41, "Product 1", 100000L, 2, 200000L));
        payments.add(new Payment(42, "Product 2", 150000L, 2, 300000L));
        payments.add(new Payment(43, "Product 3", 200000L, 1, 200000L));
        payments.add(new Payment(44, "Product 4", 250000L, 4, 1000000L));
        payments.add(new Payment(45, "Product 1", 100000L, 2, 200000L));

        return payments;
    }
}
