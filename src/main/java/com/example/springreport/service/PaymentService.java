package com.example.springreport.service;

import com.example.springreport.entity.Payment;

import java.util.List;

public interface PaymentService {

    List<Payment> paymentList();

}
